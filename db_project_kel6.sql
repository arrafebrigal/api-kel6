/*
SQLyog Ultimate v12.4.3 (64 bit)
MySQL - 10.4.17-MariaDB : Database - db_project_kel6
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_project_kel6` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `db_project_kel6`;

/*Table structure for table `data_barang` */

DROP TABLE IF EXISTS `data_barang`;

CREATE TABLE `data_barang` (
  `kode_barang` char(11) NOT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `jumlah` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_barang`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_barang` */

insert  into `data_barang`(`kode_barang`,`nama_barang`,`jumlah`) values 
('001','Monitor','25 Unit'),
('002','Meja','20 Unit'),
('003','Buku','20 Buku');

/*Table structure for table `data_pengajuan` */

DROP TABLE IF EXISTS `data_pengajuan`;

CREATE TABLE `data_pengajuan` (
  `kode_pengajuan` char(11) NOT NULL,
  `tanggal` varchar(50) DEFAULT NULL,
  `npm_peminjam` char(11) DEFAULT NULL,
  `nama_peminjam` varchar(50) DEFAULT NULL,
  `prodi` varchar(50) DEFAULT NULL,
  `no_handphone` char(15) DEFAULT NULL,
  PRIMARY KEY (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengajuan` */

insert  into `data_pengajuan`(`kode_pengajuan`,`tanggal`,`npm_peminjam`,`nama_peminjam`,`prodi`,`no_handphone`) values 
('P01','11-6-2023','20311987','Ara','SI','08213473892'),
('P02','14-6-2023','20311084','Selvi','IF','08123744672'),
('P03','17-6-2023','20311487','Salsa','IF','08213476783');

/*Table structure for table `data_pengembalian` */

DROP TABLE IF EXISTS `data_pengembalian`;

CREATE TABLE `data_pengembalian` (
  `kode_pengembalian` char(11) NOT NULL,
  `kode_pengajuan` varchar(50) DEFAULT NULL,
  `tanggal_kembali` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`kode_pengembalian`),
  KEY `kode_pengajuan` (`kode_pengajuan`),
  CONSTRAINT `data_pengembalian_ibfk_1` FOREIGN KEY (`kode_pengajuan`) REFERENCES `data_pengajuan` (`kode_pengajuan`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Data for the table `data_pengembalian` */

insert  into `data_pengembalian`(`kode_pengembalian`,`kode_pengajuan`,`tanggal_kembali`) values 
('K01','P02','14-6-2023'),
('K02','P03','18-6-2023'),
('K03','P01','21-6-2023');

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `username` varchar(50) DEFAULT NULL,
  `pass` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id_user`,`name`,`username`,`pass`,`role`) values 
(3,'selvi','user','e7de9abd2abe6288bbbc928c62ae58ad','Admin'),
(4,'ara','tes','28b662d883b6d76fd96e4ddc5e9ba780','Admin');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
